import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Auth from './Components/Auth/Auth';

class App extends Component {
  constructor(props) {
    super(props);

    var token = JSON.parse(localStorage.getItem('token'));
    if (token){
      var expiresAt = new Date(token.expires_at);
      var now = new Date(Date.now());
      var nowUtc  = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
      if (nowUtc > expiresAt) {
        localStorage.removeItem('token');
      }
    }

    this.state = {
      token: token
    };
  }

  login(token) {
    this.setState({ token : token});
    localStorage.setItem('token', JSON.stringify(token));
  }

  render() {
    return (
      <div className="App">
        {!this.state.token ?
          ( <Auth login={this.login.bind(this)} /> )
          :
          (
            'authentified'
          )
        }
      </div>
    );
  }
}

export default App;
