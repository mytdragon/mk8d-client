import React, {Component} from 'react';
import './Auth.css';
import axios from 'axios';

export default class Auth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      remember_me: false,

      errors: {
        email: false,
        password: false,
        general: false
      }
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validateAuth(){
    var validation = true;
    var email, password = false;

    if (this.state.email == ''){ email = 'Please fill email.'; }
    var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    if (!this.state.email.match(emailRegex)) { email = 'Email\'s format is invalid.'; }

    if (this.state.password == ''){ password = 'Please fill password.'; }
    if (this.state.password.length < 8) { password = 'The password must be at least 8 chars.'; }

    if (!email || !password) { validation = false; }
    this.setState({ errors : {email:email, password: password}});
    return !email && !password;
  }

  handleSubmit(event){
    event.preventDefault();

    if (!this.validateAuth()) { return; };

    axios({
      method: 'post',
      url: '/api/auth/login',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      data: {
        email: this.state.email,
        password: this.state.password,
        remember_me: this.state.remember_me
      }
    })
    .then(response => {
      console.log(response.data);
      var token = {
        access_token: response.data.access_token,
        expires_at: response.data.expires_at,
        token_type: response.data.token_type
      };

      this.props.login(token);
    })
    .catch(error => {
      switch (error.response.data.error){
        case 'InvalidCredentials':
          this.setState({ errors : {email:true, password: true, general: error.response.data.user}});
          break;

        case 'UserNotActivated':
          this.setState({ errors : {email:true, password: true, general: error.response.data.user}});
          break;

        default:
          this.setState({ errors : {email:true, password: true, general: 'An error has occured.'}});
          break;
      }
    });
  }

  render() {
    return (
      <div className="Auth login-dark">
        <form onSubmit={this.handleSubmit} noValidate="novalidate">
          <div className="form-group">
            <input onChange={e => this.setState({ email: e.target.value})} className={"form-control" + (this.state.errors.email ? ' error' : '')} type="email" name="email" placeholder="Email"/>
            <div className={"invalid-feedback" + (this.state.errors.email ? ' show' : '')}>
              {this.state.errors.email}
            </div>
          </div>
          <div className="form-group">
            <input onChange={e => this.setState({ password: e.target.value})} className={"form-control" + (this.state.errors.password ? ' error' : '')} type="password" name="password" placeholder="Password"/>
            <div className={"invalid-feedback" + (this.state.errors.password ? ' show' : '')}>
              {this.state.errors.password}
            </div>
          </div>
          <div className="custom-control custom-checkbox">
            <input onChange={e => this.setState({ remember_me: e.target.checked})} type="checkbox" className="custom-control-input" name="rememberme" id="rememberme"/>
            <label className="custom-control-label rememberme" htmlFor="rememberme">Remember me</label>
          </div>
          <div className="form-group">
            <div className={"invalid-feedback" + (this.state.errors.general ? ' show' : '')}>
              {this.state.errors.general}
            </div>
            <button className="btn btn-primary-dark btn-block" type="submit">Login</button>
          </div>
          <a href="#" className="forgot">Forgot your password?</a>
          <a href="#" className="forgot">No account?</a>
        </form>
      </div>
    )
  }
}
